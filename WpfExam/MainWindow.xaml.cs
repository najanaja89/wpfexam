﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfExam.Models;

namespace WpfExam
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
          InitializeComponent();
        }

        private void ViewButtonClick(object sender, RoutedEventArgs e)
        {
            var methods = new Methods();
            if (!methods.DigitCheck(textBox.Text))
            {
                MessageBox.Show("Input must be digit!");
                return;
            }
            else if (textBox.Text == "0")
            {
                MessageBox.Show("Input must be more than 0!");
                return;
            }
            else
            {
                var webClient = new WebClient();
                string jsonString = webClient.DownloadString($"https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&limit={textBox.Text.ToString()}&orderby=time-asc");
                var earthQuake = EarthQuake.FromJson(jsonString);
                webClient.Dispose();
                dataGrid.ItemsSource = methods.ParseJsonToDataGrid(earthQuake);

            }
        }
    }
}
