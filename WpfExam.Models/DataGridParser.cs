﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfExam.Models
{
    public class DataGridParser
    {
        public string Place { get; set; } = "";
        public string Deep { get; set; } = "";
        public string Date { get; set; } = "";
        public string Magnitude { get; set; } = "";
    }
}
