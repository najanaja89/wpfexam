﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;

namespace WpfExam.Models
{
    public class Methods
    {
        public bool DigitCheck(string digit)
        {
            if (Regex.IsMatch(digit, @"^[0-9]+$"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        //EarthQuake earthQuake
        public List<DataGridParser> ParseJsonToDataGrid(EarthQuake earthQuake)
        {
            var gridParserList = new List<DataGridParser>();
           
            foreach (var item in earthQuake.Features)
            {
                var dataGridParser = new DataGridParser();
                dataGridParser.Place = item.Properties.Place.ToString();
                dataGridParser.Magnitude = item.Properties.Mag.ToString();

                //var webClient = new WebClient();
                //string jsonString = webClient.DownloadString(item.Properties.Detail.ToString());
                //var details = JsonConvert.DeserializeObject<EarthQuakeDetails>(jsonString);
                //webClient.Dispose();
                //dataGridParser.Deep = details.Properties.Products.Origin[0].Properties.Depth;


                DateTime date = DateTime.FromFileTimeUtc(item.Properties.Time.Value);
                //here backslash is must to tell that colon is
                //not the part of format, it just a character that we want in output
                string str = date.ToString("dd/MM/yyyy ");

                dataGridParser.Date = str;
                gridParserList.Add(dataGridParser);
            }
            return gridParserList;

        }
    }
}
