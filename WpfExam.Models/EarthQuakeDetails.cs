﻿//using System;
//using System.Collections.Generic;

//public partial class EarthQuake
//{
//    public string Type { get; set; }
//    public EarthQuakeProperties Properties { get; set; }
//    public Geometry Geometry { get; set; }
//    public string Id { get; set; }

//    public static object FromJson(string jsonString)
//    {
//        throw new NotImplementedException();
//    }
//}

//public partial class Geometry
//{
//    public string Type { get; set; }
//    public List<double> Coordinates { get; set; }
//}

//public partial class EarthQuakeProperties
//{
//    public double? Mag { get; set; }
//    public string Place { get; set; }
//    public long? Time { get; set; }
//    public long? Updated { get; set; }
//    public long? Tz { get; set; }
//    public Uri Url { get; set; }
//    public object Felt { get; set; }
//    public object Cdi { get; set; }
//    public object Mmi { get; set; }
//    public object Alert { get; set; }
//    public string Status { get; set; }
//    public long? Tsunami { get; set; }
//    public long? Sig { get; set; }
//    public string Net { get; set; }
//    public string Code { get; set; }
//    public string Ids { get; set; }
//    public string Sources { get; set; }
//    public string Types { get; set; }
//    public object Nst { get; set; }
//    public object Dmin { get; set; }
//    public double? Rms { get; set; }
//    public object Gap { get; set; }
//    public string MagType { get; set; }
//    public string Type { get; set; }
//    public string Title { get; set; }
//    public Products Products { get; set; }
//}

//public partial class Products
//{
//    public List<Geoserve> Geoserve { get; set; }
//    public List<Origin> Origin { get; set; }
//    public List<Origin> PhaseData { get; set; }
//}

//public partial class Geoserve
//{
//    public long? Indexid { get; set; }
//    public long? IndexTime { get; set; }
//    public string Id { get; set; }
//    public string Type { get; set; }
//    public string Code { get; set; }
//    public string Source { get; set; }
//    public long? UpdateTime { get; set; }
//    public string Status { get; set; }
//    public GeoserveProperties Properties { get; set; }
//    public long? PreferredWeight { get; set; }
//    public GeoserveContents Contents { get; set; }
//}

//public partial class GeoserveContents
//{
//    public GeoserveJson GeoserveJson { get; set; }
//}

//public partial class GeoserveJson
//{
//    public string ContentType { get; set; }
//    public long? LastModified { get; set; }
//    public long? Length { get; set; }
//    public Uri Url { get; set; }
//}

//public partial class GeoserveProperties
//{
//    public string Eventsource { get; set; }
//    public string Eventsourcecode { get; set; }
//    public string Location { get; set; }
//    public string PdlClientVersion { get; set; }
//    public bool? TsunamiFlag { get; set; }
//    public long? UtcOffset { get; set; }
//}

//public partial class Origin
//{
//    public long? Indexid { get; set; }
//    public long? IndexTime { get; set; }
//    public string Id { get; set; }
//    public string Type { get; set; }
//    public string Code { get; set; }
//    public string Source { get; set; }
//    public long? UpdateTime { get; set; }
//    public string Status { get; set; }
//    public OriginProperties Properties { get; set; }
//    public long? PreferredWeight { get; set; }
//    public OriginContents Contents { get; set; }
//}

//public partial class OriginContents
//{
//    public GeoserveJson ContentsXml { get; set; }
//    public GeoserveJson QuakemlXml { get; set; }
//}

//public partial class OriginProperties
//{
//    public string Depth { get; set; }
//    public string EvaluationStatus { get; set; }
//    public string EventType { get; set; }
//    public string EventParametersPublicId { get; set; }
//    public string Eventsource { get; set; }
//    public string Eventsourcecode { get; set; }
//    public DateTimeOffset? Eventtime { get; set; }
//    public string Latitude { get; set; }
//    public string Longitude { get; set; }
//    public string Magnitude { get; set; }
//    public string MagnitudeType { get; set; }
//    public long? NumPhasesUsed { get; set; }
//    public string QuakemlMagnitudePublicid { get; set; }
//    public string QuakemlOriginPublicid { get; set; }
//    public string QuakemlPublicid { get; set; }
//    public string ReviewStatus { get; set; }
//    public string StandardError { get; set; }
//    public long? Version { get; set; }
//    public string VerticalError { get; set; }
//}
